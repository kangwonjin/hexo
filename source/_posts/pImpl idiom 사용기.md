---
title: pImpl idiom 사용기
date: 2019-06-28 11:33:19
tags:
---

## 알려진 장단점
Modern C++ 및 C++ API 설계 책에 pImpl Idiom에 대한 내용이 많이 나온다. (예전에 본 내용이라 추후에 팩트체크를 해봐야 한다)

### 장점
1. 사용자에게 private 변수 및 함수를 숨길 수 있다.
2. API 설계와 구현이 분리된다.
3. 코드간의 의존성을 낮추어 컴파일 시간을 줄일 수 있다.


### 단점
1. API 변경시에 고쳐야 할 부분이 많아진다.
2. 동적할당으로 인한 약간의 성능저하가 있을 수 있다.
3. 함수 호출 단계를 1 단계 더 거치므로 성능저하가 있을 수 있다.
4. cpp 파일이 더러워진다.

> 그 외 알려진 장단점에 대한 내용은 추후에 추가
---
## 사용 후기

#### **가장 큰 장점이라 생각되는 점:**

- 사용자에게 private 변수 및 함수를 숨길 수 있다.

현재 진행 중인 프로젝트에서는 라이브러리를 배포할 때 내가 사용한 라이브러리를 감추는데 이용하였다. 함수의 인터페이스를 전부 기본 타입으로 하고 cpp 파일 내부에서만 라이브러리를 사용하여 static으로 빌드하면 결과적으로 사용자에게는 어떠한 라이브러리를 사용했는지 알수 없다.

#### **가장 큰 단점이라 생각되는 점:**

- API 변경시에 고쳐야 할 부분이 많아진다.

현재 진행중인 프로젝트에서는 API 변경이 밥먹듯이 일어났다. 해당 API를 변경하기 위해서는 설계용 클래스와 구현용 클래스(Impl class)에 모두 함수를 변경해야 했다. 성능의 저하는 직접 비교하며 재어 보지는 않았지만 매우 미미할 것으로 예상된다.

#### **해결하고 싶은 점:**

- cpp 파일이 더러워진다.

cpp 파일 안에 impl class의 선언부, 구현부, design class의 구현부가 들어가게 된다. 작업하는 프로젝트에서는 private 함수들 까지 구분하려하니 하나의 cpp 파일안에 너무 많은 분류의 코드들이 들어간다. 그러다 보니 내용을 바꿀 때 해당 코드를 찾는데 직관적으로 알아보기 힘든점이 있었다.
이 문제를 해결하기 위해서 opencv aruco svmmarkers 모듈을 벤치마킹 해보았다. 

~~~cpp
namespace aruco {

\**
 * SVM Marker Detector Class
 *
 *
 *\
namespace impl{
    class SVMMarkers;
}

class   SVMMarkers: public MarkerLabeler {
    impl::SVMMarkers *_impl;
  public:

    SVMMarkers();
    virtual ~SVMMarkers(){}
    \**
     * @brief getName
     * @return
     *\
    std::string getName()const{return "SVM";}

    \\loads the svm file that detects the markers
    bool load(std::string path="") ;
    \**
     * Detect marker in a canonical image.
     * Return marker id in 0 rotation, or -1 if not found
     * Assign the detected rotation of the marker to nRotation
     *\
     bool detect(const cv::Mat &in, int & marker_id,int &nRotations,std::string &additionalInfo) ;
     int getBestInputSize();
};

}
~~~

위와 같이 namespace에 impl을 선언하여 namespace로 구분한다면 코드 수정시 design class와 impl class는 확실하게 가를 수 있다고 생각된다. 처음에는 cpp파일을 2개로 분할 해보려 했지만, 일반적으로 많이 사용된느 것 같지 않아서 포기했다.