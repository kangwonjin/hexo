---
title: C#에서 Cpp 모듈 사용하기
date: 2019-06-28 11:33:19
tags:
---

## 개요

![6109a3e5.png](/hexo/images/6109a3e5.png)

C++ 모듈 개발 -> C++/CLI 모듈 개발 -> C# Application 개발

## C++/CLI를 거치는 이유
C++는 메모리를 직접 관리하는 unmanaged code이고, C#은 가비지컬렉터가 메모리를 관리하는 managed code이다. 따라서, 두 메모리 간의 접근이 자유롭지 못하기 때문에 이 중간을 적절하게 이어줄 필요성이 있다. 이 작업을 해주는 것이 C++/CLI 라고 보면 된다.

## C++ 모듈 개발
일반적인 C++ 모듈 개발이다.

## C++/CLI 모듈 개발
이번 포스팅의 핵심이다. 일반적인 C++ 프로젝트 설정과 같이 개발한 C++ 모듈을 해당 프로젝트에 사용가능하도록 설정한다.
1. include directory
2. library directory
3. link

위 과정을 마치면 C++ 개발이 가능하다.

나는 주로 영상 데이터를 넘길 일이 많기 때문에 영상데이터만 예제로 남긴다.

### C++/CLI 모듈 코드
~~~ cpp
array<int>^ dt::MarkerDetectDesktopWrapper::decode(IntPtr img_data, const int img_width, const int img_height)
{
    std::vector<unsigned int> result;
    array<int>^ resultArray = nullptr;
    unsigned char* img = (unsigned char*)img_data.ToPointer(); // 중요한 부분
    /** 
      본래 pin_ptr를 사용하여 관리되는 메모리를 고정시키는게 중요하다. 하지만 이 예제에서는
      C#의 Bitmap.LockBit()를 사용해 고정했기때문에 사용하지 않아도 된다고 생각한다. (실제 테스트는 해보지 않음)
    */

    try {
        result = detector->decode(img, img_width, img_height); // c++ native 함수
    }
    catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
    }

    // C#을 위한 반환값 (gcnew는 가비지컬렉터에서 관리하는 메모리로 new 하겠다는 의미)
    resultArray = gcnew array<int>(result.size()); 

    for (int i = 0; i < result.size(); ++i)
        resultArray[i] = result[i];

    return resultArray;
}
~~~

## C# 프로그램
C++/CLI 모듈을 참조하여 사용하면 된다. 그리고 C++/CLI 모듈이 C++모듈에 대한 의존성을 가지고 있기 때문에 C++ 모듈 역시 같이 배포하여 사용해야 한다.


### C# 코드
~~~ csharp
Bitmap bmp = new Bitmap("A4-1.bmp");
BitmapData img_data = bmp.LockBits(
new Rectangle(0, 0, bmp.Width, bmp.Height),
System.Drawing.Imaging.ImageLockMode.ReadWrite,
System.Drawing.Imaging.PixelFormat.Format24bppRgb);

bool b = detector.initialize(1000, 1000, bmp.Width, bmp.Height, 40);
if (!b)
Console.WriteLine("Initialize Failed!");

int[] detected_ids = detector.decode(img_data.Scan0, bmp.Width, bmp.Height); // Scan0는 메모리의 첫번째 주소
~~~

아래는 opencv를 이용하는 예제
~~~ csharp
static void Main(string[] args)
{
  Console.WriteLine("start...");
  MarkerDetectDesktopWrapper detector = new MarkerDetectDesktopWrapper();

  int device_num = 0;
  VideoCapture capture = new VideoCapture(device_num);

  Mat frame = new Mat();
  capture.Read(frame);
  Console.WriteLine("width : {0}, height: {1}", frame.Cols, frame.Rows);

  bool b = detector.initialize(1000, 1000,frame.Cols, frame.Rows, 40);
  if (!b)
    Console.WriteLine("Initialize Failed!");

  while(true)
  {
    capture.Read(frame);

    IntPtr ptr = frame.Ptr(0);  // 핵심 코드
    int[] ids = detector.decode(ptr, 640, 480); // 영상의 첫번째 주소를 포인터로 반환한다.

    foreach (int id in ids)
      detector.drawMarker(ptr, 640, 480, id, 0, 255, 0, 5, true);

    Cv2.ImShow("show", frame);
    int k = Cv2.WaitKey(1);
    if (k == 'q')
    break;
  }
}
~~~

위 예제에 사용된 C++/CLI 및 C++ 모듈은 같다.