---
title: DRR Coordinate System 분석
date: 2019-06-28 11:33:19
tags:
---

## ITK 시스템

1. **source를 원점에 둔다**
2. isocenter를 CT의 중심점으로 둔다.
3. 영상평면을 [-isocenter[0], -isocenter[1], **-scd**]에 둔다. (CT의 중심에서 z축으로 -scd인 위치)
4. Transform을 정의한다.
  -  Transform * Gantry * CamShift* CamRotation
  -  Trnasform: extrinsic
  -  Gantry: Z-축 회전(y+ camera system)
  -  CamShift: [-isoc[0], **-isoc[1] + scd**, -isoc[2]]
  -  CamRotation: **x축으로 -90도 회전**(-z camera system에서 y+ camera system으로 회전)
5. **Inverse Transform**을 계산하고 source와 영상평면에 적용한다.
7. source와 영상평면간에 ray-tracing을 수행한다.
8. 계산된 값을 해당 픽셀에 저장한다.
9. 출력된 영상을 **y축으로 반전**한다.

![ITK coordinate system.PNG](/hexo/images/2337bf42.PNG)

## 변경된 시스템

1. **source를 [0,0,+scd]에 둔다**
2. isocenter를 CT의 중심점으로 둔다.
3. 영상평면을 [-isocenter[0], -isocenter[1], **-scd**]에 둔다. (CT의 중심에서 z축으로 -scd인 위치)
4. Transform을 정의한다.
  -  Transform * Gantry * CamShift* CamRotation
  -  Trnasform: extrinsic
  -  Gantry: Z-축 회전(y+ camera system) (0을 사용할 예정)
  -  CamShift: [-isoc[0], **-isoc[1]**, -isoc[2]]
  -  CamRotation: 회전 하지 않음(-z camera system 사용)
5. **Inverse Transform**을 계산하고 source와 영상평면에 적용한다.
7. source와 영상평면간에 ray-tracing을 수행한다.
8. 계산된 값을 해당 픽셀에 저장한다.
9. 출력된 영상의 위치를 +scd로 변경 (vtk에서 사용하는 system이 +z 카메라 시스템이기 때문)


## 시스템 비교

ITK 시스템
![13aa0e60.png](/hexo/images/13aa0e60.png)
[좌: 기본 시스템모습 -> 우: inverse tf 적용 후 시스템 모습]

변경된 시스템
![6461fcbb.png](/hexo/images/6461fcbb.png)
[좌: 기본 시스템모습 -> 우: inverse tf 적용 후 시스템 모습]