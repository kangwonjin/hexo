---
title: ArUco Android build
date: 2019-06-28 11:33:19
tags:
---

## ArUco 라이브러리 설치
라이브러리를 아래의 링크에서 다운로드 하고 압축파일을 푼다.
[ArUco download \| SourceForge.net](https:\\sourceforge.net\projects\aruco)

기존 프로젝트에서는 ArUco 3.0.12를 사용하여 3.0.12버전을 받으려 하였으나, old version에 대한 source를 찾을 수 없어서 3.1.0 버전을 받았다.
![ff60fe75.png](/hexo/images/ff60fe75.png)

## NDK 설치
Android studio에 있는 ndk를 사용한다.
> C:\Users\Digitrack\AppData\Local\Android\Sdk\ndk-bundle\build\cmake

## opencv 설치
아래의 링크에서 opencv의 android pack을 설치한다. 기존 프로젝트에서 opencv 3.4.2버전을 사용하고 있었으므로 3.4.2버전을 설치한다.
[Releases - OpenCV library](https:\\opencv.org\releases.html)

## ArUco CMake Configuration and Generation
ArUco 3.0.12 버전에서는 androidcompile.sh 라는 shell script파일을 같이 배포하여 쉽게 빌드 가능하였지만, 3.1.0버전에서는 보이지 않는다. 따라서 직접 CMake를 해주어야 한다. 

![e1c53964.png](/hexo/images/e1c53964.png)

위의 그림과 같이 MinGW Makefiles, Specify toolchain file for cross-compiling을 선택한다. Next 버튼을 누르면 아래와 같은 창이 나타난다.

![f07be8b8.png](/hexo/images/f07be8b8.png)

설치한 NDK안에서 android.toolchain.cmake 파일을 찾아서 선택한다.
Finish버튼을 누르고 Configure 버튼을 누른다.

그러면 여러가지 에러가 나오는데 전부 해결하면 된다. 큼직큼직한 에러만 추가적으로 설명하겠다.

**1. sh.exe와 make.exe를 못 찾음**
sh.exe, 와 make.exe를 못찾길래 sh.exe는 git에서, make.exe는 qt에서 직접 찾아주었다. 없으면 설치해야한다.
CMAKE_MAKE_PROGRAM: make.exe의 경로
CMAKE_SH: sh.exe의 경로

**2. opencv를 못 찾음**
아래와 같은 에러가 출력창에 나타나게 된다. 해당하는 플랫폼에 맞추어 변수 값을 지정해준다.
OpenCV_DIR: C:\Library\source\OpenCV-android-sdk\sdk\native\jni\abi-armeabi-v7a (OpenCVConfig.cmake 파일이 있는 위치)
~~~
CMake Error at cmake\findDependencies.cmake:5 (find_package):
  By not providing "FindOpenCV.cmake" in CMAKE_MODULE_PATH this project has
  asked CMake to find a package configuration file provided by "OpenCV", but
  CMake did not find one.

  Could not find a package configuration file provided by "OpenCV" with any
  of the following names:

    OpenCVConfig.cmake
    opencv-config.cmake

  Add the installation prefix of "OpenCV" to CMAKE_PREFIX_PATH or set
  "OpenCV_DIR" to a directory containing one of the above files.  If "OpenCV"
  provides a separate development package or SDK, be sure it has been
  installed.
Call Stack (most recent call first):
  CMakeLists.txt:14 (include)
  ~~~
  
  **3. BUILD_UTILS OFF**
  
  **4. ANDROID_STL**
  ANDROID_STL 변수를 추가하고 값을 c++_shared로 변경한다.
  
  **5. ANDROID_ABI**
  ANDROID_ABI 변수를 추가하고 값을 armeabi-v7a with NEON으로 변경한다.
  
  **6. CMAKE_INSTALL_PREFIX**
  오류는 아니지만 개인적인 취향으로 install 기본경로인 program files를 build하는 폴더 내의 install 폴더로 변경한다.
  > C:\Library\source\aruco-3.1.0\build-android\install
  > 

  **7. BUILD_SHARED_LIBS OFF**
  BUILD_SHARED_LIBS를 체크해제 한다.
  
  마지막으로 Generate 버튼을 누른다.
  
  ## Build and install
  
  build를 한 폴더로 들어가서 make를 한다.
  
  ![1f4c60ba.png](/hexo/images/1f4c60ba.png)
  
  make install 한다.
  